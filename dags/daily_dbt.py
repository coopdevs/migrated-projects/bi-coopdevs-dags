from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.models import Variable

from datetime import datetime


with DAG("DAG_daily_dbt", start_date=datetime(2021, 1, 1), schedule_interval="0 4 * * *", catchup=False) as dag:
    dbt_run = BashOperator(
        task_id='dbt_run',
        bash_command='dbt run --project-dir='+Variable.get("path_coopdevs")
                     + 'dbt/daily  --profiles-dir=' + Variable.get("path_coopdevs")+'.dbt',
        email_on_failure=True,
        email=Variable.get("mail_zulip"),
        dag=dag
    )

    dbt_run
