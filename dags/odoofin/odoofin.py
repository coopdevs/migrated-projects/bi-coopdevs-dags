from airflow.hooks.base import BaseHook
from airflow.hooks.postgres_hook import PostgresHook
from coopdevsutils.coopdevsutils import executequery


_dropkpi = "drop table if exists external.kpis;"
_createkpi = 'create table external.kpis as ' \
             'select  mrke.name as expression_name ' \
             ' , mrk."name" as kpi_name, mrk.description, mrk."sequence" , type, compare_method, accumulation_method' \
             ' , mr."name" as report_name,' \
             ' false as is_fulla ' \
             ' from odoo12_mis_report_kpi_expression mrke ' \
             '	join odoo12_mis_report_kpi mrk  on mrke.kpi_id =mrk.id ' \
             '	join odoo12_mis_report mr on mrk.report_id=mr.id;'

_updatekpileaf = "update external.kpis set is_fulla=true" \
                 " where 1=1 " \
                 " and not exists (select *" \
                 " from external.kpis b" \
                 " where external.kpis.report_name=b.report_name " \
                 " and external.kpis.expression_name like '%%'||b.kpi_name||'%%');"

_updatekpiqry = "update external.kpis set expression_name = replace(external.kpis.expression_name, b.kpi_name, b.expression_name) " \
                "from external.kpis b " \
                " where external.kpis.report_name=b.report_name" \
                "	and external.kpis.expression_name like '%%'||b.kpi_name||'%%'" \
                "	and external.kpis.expression_name not like '%%'''||b.kpi_name||'%%'" \
                "	and external.kpis.expression_name not like '%% '||b.kpi_name||'%%'" \
                "	and b.is_fulla = false;"

_updatekpiexpressionspace = "update external.kpis set expression_name=replace(expression_name, ' ','') " \
                            "where expression_name like '%% %%';"

_updatekpiexpressionminus = "update external.kpis set expression_name=replace(expression_name, '--','-') " \
                            "where expression_name like '%%--%%';"

_updatekpiexpressionplus = "update external.kpis set expression_name=replace(expression_name, '++','+') " \
                           "where expression_name like '%%++%%';"

_updatekpiexpressionminusplus = "update external.kpis set expression_name=replace(expression_name, '-+','-') " \
                                "where expression_name like '%%-+%%';"

_updatekpiexpressionplusminus = "update external.kpis set expression_name=replace(expression_name, '+-','-') " \
                                "where expression_name like '%%+-%%';"

_dropkpiexpressions = "drop table if exists external.aux_kpi_expressions;"


_dropaccountmovelinered = "drop table if exists external.account_move_line_red;"

_createaccountmovelinered = "create table external.account_move_line_red as" \
                            " select am.date, aml.balance, aa.code, aml.company_id, aa.name as account_name " \
                            "from odoo12_account_move am " \
                            "   join odoo12_account_move_line aml on am.id=aml.move_id " \
                            "join odoo12_account_account aa on aml.account_id =aa.id " \
                            " where state not in ('draft');"

_groupby = "' date, company_id, code, account_name '"


_from = "'from odoo12_account_move am" \
        " join odoo12_account_move_line aml on am.id=aml.move_id" \
        " join odoo12_account_account aa on aml.account_id =aa.id ' "

_from = "'from external.account_move_line_red ' "


_createkpiexpressions = "create table external.aux_kpi_expressions as " \
                        " select expression_name,kpi_name,description,\"sequence\",\"type\",compare_method" \
                        " ,accumulation_method,report_name,is_fulla,unnest_expr,camps_group,fr" \
                        "," \
                        "'select '||camps_group ||', '||" \
                        " case when unnest_expr like '%%+%%' then '1*' when unnest_expr like '%%-%%' then '-1*' else '1*' end " \
                        " ||accumulation_method||" \
                        "		case" \
                        "				when unnest_expr ~ '[-+]?bal+(.)' then '(balance) as balance'" \
                        "				when unnest_expr ~ '[-+]?crd+(.)' then '(credit) as balance'" \
                        "				when unnest_expr ~ '[-+]?deb+(.)' then '(debit) as balance'" \
                        "				end" \
                        "		|| E'\r\n' ||" \
                        "		fr" \
                        "		|| E'\r\n' ||" \
                        "		 case when first_sentence is not null and first_sentence<>''" \
                        "			then  'where '  else '' end||" \
                        "		case when first_sentence is not null and first_sentence<>''" \
                        "			then" \
                        "			'(code like '''|| replace(first_sentence,',', ''' or code like ''')||''')'" \
                        "			else" \
                        "			'' end" \
                        "		|| E'\r\n' ||'group by ' ||camps_group" \
                        "		as qry" \
                        ",replace(replace(SUBSTRING(unnest_expr from '(\]\[(.)+\])'),']',''),'[','') as second_sentence" \
                        " from (" \
                        "	select expression_name,kpi_name,description,\"sequence\",\"type\",compare_method" \
                        ",accumulation_method,report_name,is_fulla,unnest_expr,camps_group,fr" \
                        "	,SUBSTRING( SUBSTRING(unnest_expr from '(\[(.)+\])'),2, POSITION(']'in SUBSTRING(unnest_expr from '(\[(.)+\])'))-2) as first_sentence " \
                        " from (" \
                        "	select" \
                        "	expression_name,kpi_name,description,\"sequence\",\"type\",compare_method" \
                        " ,accumulation_method,report_name,is_fulla" \
                        "	,unnest(string_to_array(replace(replace(expression_name, '+', '*+') , '-', '*-'),'*')) as unnest_expr" \
                        "	," + _groupby + " as camps_group" \
                        "	, " + _from + " as fr" \
                        "	from external.kpis" \
                        "	) b" \
                        ") a" \
                        " where unnest_expr not in (' ','');"



_updatekpiexpressions = "update external.aux_kpi_expressions set qry=b.qry" \
                        " from external.aux_kpi_expressions b" \
                        " where external.aux_kpi_expressions.qry is null" \
                        "	and  external.aux_kpi_expressions.report_name=b.report_name " \
                        " and trim(external.aux_kpi_expressions.unnest_expr) like '_'||b.kpi_name;"

_finaltablecalculation = "DO \r" \
                         " $do$ \r" \
                         "    BEGIN \r" \
                         " 	execute ( \r" \
                         "		select 'drop table if exists external.fin_odoo;  \r create table  external.fin_odoo as ' || string_agg(qry_final, E'\r\n'||' union all '||E'\r\n') ||';'  \r" \
                         "		from ( \r" \
                         "			select description, report_name, \"sequence\" \r" \
                         "			,  string_agg(replace(qry, 'select ', \r" \
                         "				'select '''||report_name ||''' as report_name, '''||description ||''' as description, '''||\"sequence\" ||''' as seq, ' ) , E'\r\n'||' union all '||E'\r\n') \r" \
                         "				as qry_final \r" \
                         "			from external.aux_kpi_expressions ake \r" \
                         "			where 1=1 \r" \
                         "	and qry is not null \r" \
                         "			group by description, report_name, \"sequence\" \r" \
                         "	) a \r" \
                         "	); \r" \
                         "   END \r" \
                         " $do$; \r"


def calculaodoofin():
    conndwh = BaseHook.get_connection('DWH').get_hook().get_sqlalchemy_engine()
    executequery(_dropaccountmovelinered, conndwh)
    executequery(_createaccountmovelinered, conndwh)

    executequery(_dropkpi, conndwh)
    executequery(_createkpi, conndwh)
    executequery(_updatekpileaf, conndwh)
    for x in range(20):
        executequery(_updatekpiqry, conndwh)
    executequery(_updatekpiexpressionspace, conndwh)
    for x in range(20):
        executequery(_updatekpiexpressionminus, conndwh)
    for x in range(20):
        executequery(_updatekpiexpressionplus, conndwh)
    for x in range(20):
        executequery(_updatekpiexpressionminusplus, conndwh)
    for x in range(20):
        executequery(_updatekpiexpressionplusminus, conndwh)
    executequery(_dropkpiexpressions, conndwh)
    executequery(_createkpiexpressions, conndwh)
    executequery(_updatekpiexpressions, conndwh)

    conndwh2 = PostgresHook(postgres_conn_id='DWH').get_conn()
    engine = conndwh2.cursor()
    engine.execute('call external.odoo_fin();')
    conndwh2.commit()
    engine.close()
    conndwh2.close()

